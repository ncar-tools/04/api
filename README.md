<img height="86" align="right" src="gfx/ncar-logo.svg" alt="logo"/><sub>NeuroCar / Documentation / API</sub>

# [<img height=24 src="gfx/book-open-solid.svg">](#) NeuroCar API – Documentation – Version 4<!-- omit in toc -->

> General concept, data structures and interface functions for NeuroCar system components.

---

- [1. Basics](#1-basics)
  - [1.1. Microservices](#11-microservices)
  - [1.2. Architecture](#12-architecture)
  - [1.3. Security](#13-security)
  - [1.4. API](#14-api)
  - [1.5. GUI](#15-gui)
  - [1.6. Units](#16-units)
  - [1.7. Date & time](#17-date--time)
  - [1.8. Versioning](#18-versioning)
  - [1.9. Language](#19-language)
- [2. Data structures](#2-data-structures)
  - [2.1. Vehicles](#21-vehicles)

## 1. Basics

### 1.1. Microservices

NeuroCar utilizes the concept of [microservices](https://en.wikipedia.org/wiki/Microservices), an all elements of the NeuroCar protocol are based on open data formats. The elements communicate with each other using HTTP(s) to send commands, data records, and messages. Communication is based on per-to-per connections (no data bus) in either IPv4 or IPv6 infrastructure.

### 1.2. Architecture

![ncar-architecture](gfx/ncar-arch.svg)

The measurement devices (cameras, sensors, etc.) are connected to a local data concentrator – the **Terminal**. The terminal is equipped with the controller (CPU) which processes the measurement data and forms the data records in compliance with NeuroCar specification. Such prepared data are sent from the terminal to the central system – **BackOffice**, using the HTTP(s) protocol. For this purpose, a special endpoint (URL) is prepared in the central system dedicated to receive measurement data, i.e. `API-INT` (*internal API*). Users of the system can access it through a separate "external" endpoint `API` (*external API*).

### 1.3. Security

A standard set of protocols is used to handle the connections between system components to provide security for HTTP transmission ([OpenSSL library](https://en.wikipedia.org/wiki/OpenSSL)). The basic protocol is [TLS](https://en.wikipedia.org/wiki/Transport_Layer_Security) (the minimal version is TLS 1.2 → the TLS 1.3 is recommended). TLS typically relies on a set of trusted [X.509](https://en.wikipedia.org/wiki/X.509) customer certificate authorities to establish the authenticity of certificates. In general constructing the ultimate secure system therefore requires implementation of a [public key infrastructure](https://en.wikipedia.org/wiki/Public_key_infrastructure). It is possible to decrease security, e.g. by using self-signed certificates, or by disabling encryption at all (e.g. when communication is within one kubernetes cluster).

### 1.4. API

The system functions can be accessed through a dedicated API, which is described in this project. NeuroCar API specification was written in [OpenAPI 3.1.0 standard](https://www.openapis.org/about)

### 1.5. GUI

The basic user interface for all applications in NeuroCar systems is the [WEB interface](https://en.wikipedia.org/wiki/Web_application). The user uses a standard web browser to connect to the system (Terminal, BackOffice) – a browser supporting [HTML 5](https://en.wikipedia.org/wiki/HTML5) is required.

### 1.6. Units

All measurements of physical values, stored in data structures, are represented in [SI base units](https://en.wikipedia.org/wiki/SI_base_unit) or [SI derived units](https://en.wikipedia.org/wiki/SI_derived_unit) – when applicable. The applications (e.g. through a GUI) can present measurement values in user-convenient units (such as miles, pounds, km/h, etc.).

### 1.7. Date & time

All measurements are provided with timestamps expressed as [UTC time](https://en.wikipedia.org/wiki/Coordinated_Universal_Time) signatures. In addition, measurements can include information about the time zone of the data collection which allows for conversion of UTC time to local time. All system components should always have their internal clocks properly synchronized with as much accuracy as possible. It is recommended to use the [NTP protocol](https://en.wikipedia.org/wiki/Network_Time_Protocol). This method achieves a synchronization accuracy on the order of ±1 ms. If the synchronization accuracy must be higher, the [PTP protocol](https://en.wikipedia.org/wiki/Precision_Time_Protocol) or mechanisms using time sources in satelite navigation systems can be used (like GPS [PPS](https://en.wikipedia.org/wiki/Global_Positioning_System#Accuracy)).

### 1.8. Versioning

The entire API and its individual components are versioned according to [Semantic Versioning 2.0](https://semver.org/lang/pl/) methodology. It is assumed that the current major version of the API is **version 4**, which will be the major version of each component as well. 

### 1.9. Language

It was assumed that all elements of the NeuroCar API have names derived from the English (US) language. The same language is also used for all internal descriptions, including those contained in the specification of individual elements. Finally, this document is also prepared exclusively in English (in order to avoid ambiguity).

## 2. Data structures

### 2.1. Vehicles

The key information processed in the system is the information about recorded vehicles - **VehicleTrace**. A single VehicleTrace data record contains a set of details about a single vehicle, captured at a specific location at a specific time.

Click [here](./schemas/vehicles/openapi.json) to see OpenAPI specification related to vehicles.

---

<small>© 1992-2021 NeuroCar Sp. z o.o. – https://www.neurocar.pl</small>